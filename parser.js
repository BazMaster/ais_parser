// https://github.com/puppeteer/puppeteer/issues/4985 - antibot instructions

const puppeteer = require('puppeteer');
const mysql = require('mysql2');
const mkdirp = require('mkdirp')
const moment = require('moment');
require('dotenv').config();

const dt = moment();
const year  = dt.format('YYYY');
const month = dt.format('MM', { trim: false });
let path = './public/uploads/screenshots/' + year + '/' + month;

if(process.env.DB_DATABASE === undefined) {
    require('dotenv').config({path: '/var/www/amazon_parser/.env'});
    path = '/var/www/amazon_parser/public/uploads/screenshots/' + year + '/' + month;
}

const db = {
    connectionLimit: 100,
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD
};

const pool = mysql.createPool(db).promise();
let sql = `SELECT id, name, link FROM links WHERE disabled = 0`;

pool.execute(sql)
    .then(result =>{
        console.log('---------------THEN')
        console.log(result[0]);
        let results = result[0];

        (async () => {
            for(let obj_key in results) {
                let obj = results[obj_key];

                if(obj.hasOwnProperty('link') && obj.hasOwnProperty('id')) {
                    // console.log('obj: ', obj);

                    let url = obj['link'];
                    let link_id = obj['id'];

                    const image_name = 'link-' + link_id + '--' + dt.format('DD_HH-mm-ss', { trim: false }) + '.jpg';
                    const created_at = dt.format('YYYY-MM-DD HH:mm:ss', { trim: false });

                    console.log(image_name);

                        try {
                            const browser = await puppeteer.launch({
                                headless: true,
                                args: ['--no-sandbox', '--disable-setuid-sandbox'],
                                ignoreDefaultArgs: ['--disable-extensions']
                            });
                            console.log(await browser.version());
                            const page = await browser.newPage();
                            await page.setViewport({
                                width: 1366,
                                height: 768,
                            });
                            // await page.waitForNavigation({waitUntil: 'domcontentloaded'});
                            await page.goto(url, {
                                // waitUntil: 'networkidle2'
                                waitUntil: 'domcontentloaded'
                            });
                            await page.waitForTimeout(1500);
                            const selector  = '#a-autoid-0'; // cookies

                            try {
                                await page.waitForSelector(selector, { timeout: 5000 });
                                await page.click(selector);
                                console.log("Cookies window closed!")
                            } catch (error) {
                                console.log("The cookies window didn't appear.")
                            }

                            await mkdirp(path).then(made => console.log(`made directories, starting with ${made}`))
                            await page.screenshot({path: path + '/' + image_name, fullPage: true});
                            let data = await page.evaluate(() => {
                                let price = document.querySelector('#priceblock_ourprice');
                                let price_sale = document.querySelector('#priceblock_saleprice');
                                let price_l = document.querySelector('#priceblock_ourprice + span');
                                let price_value, price_sale_value, price_l_value = null;


                                if (price) {
                                    price_value = price.innerText;
                                }
                                if (price_sale) {
                                    price_sale_value = price_sale.innerText;
                                }
                                if (price_l) {
                                    price_l_value = price_l.innerText;
                                }

                                return {
                                    price_value,
                                    price_sale_value,
                                    price_l_value
                                }
                            });

                            await browser.close();

                            let has_error = 0;
                            let description = '';
                            if(data.price_value) {
                                description += '<div><strong>Price:</strong> ' + data.price_value + '</div>';
                            }
                            if(data.price_sale_value) {
                                description += '<div><strong>Sale price:</strong> ' + data.price_sale_value + '</div>';
                            }
                            if(data.price_l_value) {
                                description += '<div><strong>Price/L:</strong> ' + data.price_l_value + '</div>';
                            }
                            else {
                                has_error = 1;
                            }
                            let sql = `INSERT INTO monitorings(link_id, screenshot, description, has_error, created_at, updated_at)
                                    VALUES('${link_id}', '${image_name}', '${description}', '${has_error}', '${created_at}', '${created_at}')`;

                            console.log('sql: ', sql);


                            await pool.execute(sql);

                            console.log('sql executed');

                            // return data;
                        } catch (err) {

                            await (async () => {
                                let message = err.message;
                                if(err.code) {
                                    message += ' (' + err.code + ')';
                                }
                                const created_at = dt.format('YYYY-MM-DD HH:mm:ss', {trim: false});

                                let sql = `INSERT INTO errors(message, stack,
        created_at, updated_at) VALUES('${message}', '${err.stack}', '${created_at}', '${created_at}')`;

                                await pool.execute(sql);
                                await pool.end();
                                console.log("Pull closed");
                            })();

                            throw new Error(err);
                        }


                }

            }
            await pool.end();
            console.log("Pull closed");

        })();

    })
    .catch(function(err) {
        console.log('---------------CATCH')
        console.log(err.code);
        console.log(err.message);
        console.log(err.stack);

        console.log('sql: ', sql);

        (async () => {
            let message = err.message;
            if(err.code) {
                message += ' (' + err.code + ')';
            }
            const created_at = dt.format('YYYY-MM-DD HH:mm:ss', { trim: false });

            let sql = `INSERT INTO errors(message, stack,
        created_at, updated_at) VALUES('${message}', '${err.stack}', '${created_at}', '${created_at}')`;

            await pool.execute(sql);
            await pool.end();
            console.log("Pull closed");
        })();

    });






// debugger;

// return false;
