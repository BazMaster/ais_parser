<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Link;
use App\Models\Monitoring;
use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $emails = Setting::select('value')->where('key','emails')->first();
        $emails_enabled = Setting::select('value')->where('key','emails_enabled')->first();
        $telegram_bot = Setting::select('value')->where('key','telegram_bot')->first();
        $telegram_chats = Setting::select('value')->where('key','telegram_chats')->first();
        $telegram_enabled = Setting::select('value')->where('key','telegram_enabled')->first();

        $monitorings = Monitoring::selectRaw('count(*) as total')
            ->selectRaw("count(case when has_error = '1' then 1 end) as has_error")
            ->first();
        $links = Link::selectRaw('count(*) as total')->first();

        $email_notificaiton = (($emails && $emails_enabled) && ($emails->value && $emails_enabled->value));
        $telegram_notification = (
            ($telegram_bot && $telegram_chats && $telegram_enabled) &&
            ($telegram_bot->value && $telegram_chats->value && $telegram_enabled->value)
        );

        $response = [
            'monitoring' => $monitorings->total,
            'monitoring_errors' => $monitorings->has_error,
            'links' => $links->total,
            'email_notificaiton' => $email_notificaiton,
            'telegram_notification' => $telegram_notification,
        ];
        return response()->json($response, 200);
    }


}
