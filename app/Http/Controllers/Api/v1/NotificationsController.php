<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function index()
    {
        $emails = Setting::select('value')->where('key','emails')->first();
        $emails_enabled = Setting::select('value')->where('key','emails_enabled')->first();
        $emails_enabled_val = $emails_enabled ? $emails_enabled->value : 0;
        $telegram_bot = Setting::select('value')->where('key','telegram_bot')->first();
        $telegram_chats = Setting::select('value')->where('key','telegram_chats')->first();
        $telegram_enabled = Setting::select('value')->where('key','telegram_enabled')->first();
        $telegram_enabled_val = $telegram_enabled ? $telegram_enabled->value : 0;

        $response = [
            'emails' => $emails ? $emails->value : null,
            'emails_enabled' => (boolean) $emails_enabled_val,
            'telegram_bot' => $telegram_bot ? $telegram_bot->value : null,
            'telegram_chats' => $telegram_chats ? $telegram_chats->value : null,
            'telegram_enabled' => (boolean) $telegram_enabled_val,
        ];
        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
        $arr = [];
        foreach($request->all() as $key => $value) {
            $arr[] = Setting::updateOrCreate(
                ['key' => $key],
                ['value' => $value]
            );
        }

        return response()->json($arr, 200);
    }

}
