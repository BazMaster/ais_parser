<?php

namespace App\Console\Commands;

use App\Models\Error;
use App\Models\Setting;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class ErrorHandling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:error';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notification when parsing errors occurred';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        print PHP_EOL . "=========== START ===========" . PHP_EOL;

        $errors = Error::whereNull('sent_date')->get();
        $count = $errors->count();

        print $count . " errors occured" . PHP_EOL;

        if($count) {
            foreach($errors as $error) {
                $this->toEmail($error);
                $this->toTelegram($error);
                $error->sent_date = now();
                $error->save();
            }
        }

        print PHP_EOL . "============ END ============" . PHP_EOL;

    }


    public function toEmail($error) {
        $emails = Setting::select('value')->where('key','emails')->first();
        $emails_enabled = Setting::select('value')->where('key','emails_enabled')->first();
        $emails_enabled_val = $emails_enabled ? $emails_enabled->value : 0;

        if($emails && $emails_enabled_val) {
            if(!$error->email_sended) {
                $emails = explode(',', $emails->value);

                foreach($emails as $email) {
                    Mail::send('emails.error', [
                        'error' => $error
                    ], function ($m) use ($email) {
                        $m->from(config('mail.from.address'), config('mail.from.name'));

                        $m->to($email, 'Admin')->subject('ERROR from Amazon Parser');
                    });

                    $error->email_sent = 1;
                    $error->save();
                    print PHP_EOL . 'The email message was sent successfully!' . PHP_EOL;

                }
            }
            else {
                print PHP_EOL . 'Email notification sent earlier.' . PHP_EOL;
            }
        }
        else {
            print PHP_EOL . 'Email notification is disabled' . PHP_EOL;
        }
    }

    public function toTelegram($error) {
        $screenshot = config('app.url') . str_replace('/uploads', 'uploads', $error->screenshot);
        $msg = (string) view()->make('emails.error', [
            'error' => $error
        ]);
        $msg = strip_tags($msg);

        $token = Setting::select('value')->where('key','telegram_bot')->first();
        $telegram_chats = Setting::select('value')->where('key','telegram_chats')->first();
        $telegram_enabled = Setting::select('value')->where('key','telegram_enabled')->first();
        $telegram_enabled_val = $telegram_enabled ? $telegram_enabled->value : 0;

        if($token && $telegram_chats && $telegram_enabled_val) {
            if($token->value && $telegram_chats->value) {
                if(!$error->telegram_sended) {
                    $token = $token->value;
                    $telegram_chats = explode(',', $telegram_chats->value);

                    $response = [];
                    foreach($telegram_chats as $chat_id) {
                        $response[] = Http::post('https://api.telegram.org/bot' . $token . '/sendMessage', [
                            'parse_mode' => 'html',
                            'chat_id' => $chat_id,
                            'text' => $msg,
                        ]);
                    }

                    $error->telegram_sent = 1;
                    $error->save();

                    print PHP_EOL . 'The telegram message was sent successfully!' . PHP_EOL;
                }
                else {
                    print PHP_EOL . 'Telegram notification sent earlier' . PHP_EOL;
                }
            }
        }
        else {
            print PHP_EOL . 'Telegram notification is disabled' . PHP_EOL;
        }

    }

}
