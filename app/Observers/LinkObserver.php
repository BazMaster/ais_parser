<?php

namespace App\Observers;

use App\Models\Link;

class LinkObserver
{

    /**
     * Handle the Link "deleted" event.
     *
     * @param  \App\Models\Link  $link
     * @return void
     */
    public function deleting(Link $link)
    {
        $monitorings = $link->monitoring()->get();

        foreach($monitorings as $monitoring) {
            $screenshot = $monitoring->screenshot;
            $image = public_path($screenshot);

            if(file_exists($image) && !is_dir($image)) {
                unlink($image);
            }
        }

    }


}
