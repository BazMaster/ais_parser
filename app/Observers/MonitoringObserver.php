<?php

namespace App\Observers;

use App\Models\Monitoring;

class MonitoringObserver
{
    public function deleting(Monitoring $monitoring)
    {

        $screenshot = $monitoring->screenshot;
        $image = public_path($screenshot);

        if(file_exists($image) && !is_dir($image)) {
            unlink($image);
        }

    }

}
