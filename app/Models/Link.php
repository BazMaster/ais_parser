<?php

namespace App\Models;

use App\Observers\LinkObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'link',
        'disabled',
    ];


    protected $casts = [
        'disabled' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();
        parent::observe(LinkObserver::class);
    }

    public function monitoring() {
        return $this->hasMany('App\Models\Monitoring', 'link_id', 'id');
    }
}
