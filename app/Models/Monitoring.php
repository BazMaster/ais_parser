<?php

namespace App\Models;

use App\Observers\MonitoringObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Link;

class Monitoring extends Model
{
    use HasFactory;

    protected $fillable = [
        'link_id',
        'screenshot',
        'description',
        'has_error',
    ];

    protected $appends = [
        'date',
        'time',
    ];

    protected static function boot()
    {
        parent::boot();
        parent::observe(MonitoringObserver::class);
    }

    public function link()
    {
        return $this->belongsTo(Link::class, 'link_id', 'id');
    }

    public function getDateAttribute()
    {
        return date('Y-m-d', strtotime($this->created_at));
    }

    public function getTimeAttribute()
    {
        return date('H:i', strtotime($this->created_at));
    }

    public function getScreenshotAttribute($value)
    {
        $year = date('Y', strtotime($this->created_at));
        $month = date('m', strtotime($this->created_at));
        $path = '/uploads/screenshots/' . $year . '/' . $month . '/';
        $image = $path . $value;

        if(file_exists(public_path($image))) {
            $value = $image;
        }
        else {
            $value = null;
        }
        return $value;
    }
}
