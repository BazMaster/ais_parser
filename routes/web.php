<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');

Auth::routes([
//    'register' => false,
    'password' => false,
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('/home')->middleware('auth')->group(function() {
    Route::get('{any}', function() {
        return view('index');
    })->where('any', '.*');
});

Route::prefix('/api')->middleware('auth')->group(__DIR__ . '/api_dashboard.php');
