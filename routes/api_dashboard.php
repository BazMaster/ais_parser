<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\LinksController;
use App\Http\Controllers\Api\v1\NotificationsController;
use App\Http\Controllers\Api\v1\MonitoringsController;
use App\Http\Controllers\Api\v1\HomeController;

Route::resource('links', LinksController::class);
Route::resource('notifications', NotificationsController::class);
Route::resource('monitoring', MonitoringsController::class);
Route::resource('home', HomeController::class);
