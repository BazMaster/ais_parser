import VueRouter from 'vue-router';
import Vue from 'vue';

import Home from './views/Home';
import Monitoring from './views/Monitoring';
import Links from './views/Links';
import Notifications from './views/Notifications';

Vue.use(VueRouter);

const routes = [
    {
        path: '/home',
        component: Home
    },
    {
        path: '/home/monitoring',
        component: Monitoring
    },
    {
        path: '/home/links',
        component: Links
    },
    {
        path: '/home/notifications',
        component: Notifications
    }
];

export default new VueRouter({
    mode: 'history',
    routes
});
