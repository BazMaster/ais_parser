<h1>An error occurred during Amazon parsing.</h1>

<ul>
    <li><strong>Date: </strong> {{ $error->created_at }}</li>

    <li><strong>Message: </strong> {{ $error->message }}</li>
</ul>

@if($error->stack) {
    Stack trace:
    <code>{{ $error->stack }}</code>
@endif
