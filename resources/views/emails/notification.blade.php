<p>Link #{{ $monitoring->link_id }} has something wrong.</p>

<ul>
    <li><strong>Screenshot: </strong> {{ $screenshot }}</li>

    <li><strong>Link: </strong> {{ $monitoring->link->link }}</li>

    <li><strong>Check in admin panel: </strong> {{ config('app.url') }}home/monitoring</li>
</ul>
